##########################################
# Dockerfile to change from root to 
# non-root privilege
###########################################

# Base image is python:3.9.7
FROM python:3.9.7

# Add a new user "python397" with 2861 user id
RUN useradd -u 2861 python397

# set up workdir
WORKDIR /home/python397/app

# set up read, write and execute on workdir, change ownner of workdir
RUN chmod -R u=r+w+x,g=-,o=- . \
&& chown -R python397:python397 .

# Copy requirements file to workdir
COPY ./requirements_lucas .

# install  requirements
RUN  pip install -r ./requirements_lucas

# Change user (non-root privilege)
USER python397