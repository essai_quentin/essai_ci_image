from datetime import datetime
from dateutil.parser import parse as duparse
import re



class FileUtils:
    
    @staticmethod
    def parse_date(dtstr: str, *args, **kwargs):
        """
        Parse a string and try to find a datetime info

        Parameters:
        -----------
        dtstr: str
               string to parse 
        """
        try:
            if len(dtstr) == 8 and dtstr.isnumeric():
                return datetime.strptime(dtstr, '%Y%m%d')
            else:
                return duparse(dtstr, fuzzy=True, *args, **kwargs)
        except Exception as message:
            print(message)
            return "ParserError"

    @staticmethod
    def get_extension(filename: str) -> str:
        """
        Return the extension of the file

        Parameters:
        -----------
        filename: str
                  file name where to check extension
        """
        split = filename.split(".")
        if len(split) < 2:
            msg = "the string pass as a filename does not contain an extension"
            raise Exception(msg)
        return split[-1]

    @staticmethod
    def get_info_from_filename(filename: str,
                               split_by: str = "_",
                               **kwargs) -> dict:
        """
        Function to extract values from the file name

        Parameters:
        -----------
        filename: str
                  The file name that need to be explore

        split_by: str
                  symbol used in the file name to split different names, the 
                  default value is ("_")
        kwargs: 
                The kwargs dateutil.parser.

        Returns: 
        dict
            A dictionary with the keys: words, numbers, date, version, extension
            words (list): all words included in the filename
            numbers (list): all numbers included in the filename 
            date (datetime): date time format if found in filename \
                            else "not given".           
        """
        # in case we have a full path
        if "/" in filename:
            filename = filename.split('/')[-1]
        # find words in filename
        words = [val for val in filename.split(split_by) 
                 if not val.isnumeric()][:-1]
        # find numbers in filename
        numbers = re.findall(f"\d+", filename)
        
        # get version and extension
        regex = f"{split_by}[V|v]?([\d][-|.|{split_by}]?)+\d*"
        try:
            extension = FileUtils.get_extension(filename)
            version = re.search(regex, filename.rsplit(".", 1)[0])[0]
        except:
            extension = "not given"
            version = re.search(regex, filename)[0]
        # get date if present
        succes = False

        expected_date = [number for number in numbers 
                         if len(number)>=6]
        for number in expected_date:
            date = FileUtils.parse_date(number, **kwargs)
            if date !=  "ParserError":
                succes= True
                break
        if not succes:
            for word in words:
                date = FileUtils.parse_date(word, **kwargs)
                if date !=  "ParserError":
                    succes= True
                    break

        if not succes:
            date = "not defined or unable to autoamtically find"

        values = {"words": words,
                  "numbers": numbers,  
                  "date": date,
                  "version": version.strip("_"),
                  "extension": extension}
        return values