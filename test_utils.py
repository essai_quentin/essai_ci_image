import unittest
import datetime
from utils import FileUtils
from parameterized import parameterized


class TestFileUtils(unittest.TestCase):
    """
    test for FileUtils Class
    """
    def __init__(self, *args, **kwargs):
        super(TestFileUtils, self).__init__(*args, **kwargs)
        self.get_info_from_filename_cases = {
            '20220302_FoodPilotSiRh_V1-1.CSV': 
            {'words': ['FoodPilotSiRh'],
             'numbers': ['20220302', '1', '1'],
             'date': datetime.datetime(2022, 3, 2, 0, 0),
             'version': 'V1-1',
             'extension': 'CSV'},
            '03-02-2022_Food_Pilot_SiRh_V4_5_3.txt':
            {'words': ['03-02-2022', 'Food', 'Pilot', 'SiRh', 'V4'],
             'numbers': ['03', '02', '2022', '4', '5', '3'],
             'date': datetime.datetime(2022, 3, 2, 0, 0),
             'version': 'V4_5_3',
             'extension': 'txt'},
             '03-22-2022_FoodPilot_SiRh_V4.5.3.txt':
            {'words': ['03-22-2022', 'FoodPilot', 'SiRh'],
             'numbers': ['03', '22', '2022', '4', '5', '3'],
             'date': datetime.datetime(2022, 3, 22, 0, 0),
             'version': 'V4.5.3',
             'extension': 'txt'},
            '03222022_FoodPilot_SiRh_V4.5.3.txt':
            {'words': ['FoodPilot', 'SiRh'],
             'numbers': ['03222022', '4', '5', '3'],
             'date': datetime.datetime(2022, 3, 22, 0, 0),
             'version': 'V4.5.3',
             'extension': 'txt'}}

    @parameterized.expand([["20220302_FoodPilotSiRh_V1-1.CSV"],
                           ["03-02-2022_Food_Pilot_SiRh_V4_5_3.txt"],
                           ['03-22-2022_FoodPilot_SiRh_V4.5.3.txt'],
                           ['03222022_FoodPilot_SiRh_V4.5.3.txt']])
    def test_get_info_from_filename(self, filename):
        output = FileUtils.get_info_from_filename(filename)
        self.assertDictEqual(self.get_info_from_filename_cases[filename],
                             output)

    # def _check_equal_dict(self, dict1, dict_2):
    #     same_keys = dict1.keys() == dict_2.keys()
    #     print()

if __name__ == '__main__':
    unittest.main()