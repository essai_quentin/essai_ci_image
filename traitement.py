#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import
import os
import filecmp
from random import choice
  

# variable
fichier = './text.txt'
fichier_traiter = './text_sorted.txt'



# class Traitement:


def create_randomfile():
    os.system(f"echo ' => Etape 1 :'")

    with open(fichier, "w+") as file:
        for nb_line in range(choice(range(10000, 1000000))):
            word = ""
            for nb_letter in range(choice(range(3, 51))):
                letter = choice(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k","l", "m", "n", "o", "p", "q", "r", "s", "u", "w", "x", "y", "z"])
                word += letter
            file.write(word+"\n")
    
    condition = f'file created text.txt' if os.path.isfile(fichier) else 'file created text.txt'
    os.system(f"echo {condition}")

    return True if os.path.isfile(fichier) else False

def load_sorted_list(words_list,number):
    os.system(f"echo ' => Etape 5 :'")
    # os.system(f"echo '{number}'")

    with open(fichier_traiter, "w") as file:
        for line in range (number):
            file.write(words_list[line]+"\n")

    condition = f'file created text_sorted.txt' if os.path.isfile(fichier_traiter) else 'error when creating file'
    os.system(f"echo {condition}")

    return True if os.path.isfile(fichier_traiter) else False


def count_line():
    os.system(f"echo ' => Etape 2 :'")
    numberOfLine = 0

    with open(fichier, "r") as file:
        for line in file:
            numberOfLine += 1

    os.system(f"echo 'line in file => {numberOfLine}'")

    return numberOfLine

def get_words():
    os.system(f"echo ' => Etape 3 :'")
    words = list()

    with open(fichier, "r") as file:
        for line in file:
            sub_str = line[0:len(line)-2:1]
            # print(sub_str)
            words.append(sub_str)

    os.system(f"echo 'line extracted'")

    return words


def sorting_words_list(words_list):
    os.system(f"echo ' => Etape 4 :'")
    # os.system(f"echo 'before sorting : {words_list}'")
    os.system(f"echo 'sorting line'")
    words_list.sort()
    # os.system(f"echo 'after sorting : {words_list}'")

    return words_list

def check_file_different():
    os.system(f"echo ' => Etape 6 :'")
    condition = f'files are same' if filecmp.cmp(fichier, fichier_traiter) else 'files are different'

    os.system(f"echo  {condition}")
    os.system(f"echo  '-----'")


    taille_initiale = os.stat(fichier)[6]/1024.0**2
    taille_finale = os.stat(fichier_traiter)[6]/1024.0**2
    os.system(f"echo  'Taile du fichier initial     => {taille_initiale} Mo'")
    os.system(f"echo  'Taille du fichier Trié       => {taille_finale} Mo'")

    return True if filecmp.cmp(fichier, fichier_traiter) else False


def scenario_test():
    create_randomfile()
    numberOfLine = count_line()
    words_list = get_words()
    word_list_sorted = sorting_words_list(words_list)
    load_sorted_list(word_list_sorted, numberOfLine)
    check_file_different()

if __name__ == "__main__":
    scenario_test()